•	gcloud compute networks create taw-custom-network --subnet-mode custom 

La commande crée un nouveau réseau personnalisé nommé "taw-custom-network" avec la possibilité de créer des sous-réseaux personnalisés à l'intérieur de ce réseau.

•	gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.0.0.0/16 

La commande crée un sous-réseau nommé "subnet-Region" au sein du réseau personnalisé "taw-custom-network", dans une région spécifique, avec une plage d'adresses IP définie. Ce sous-réseau peut ensuite être utilisé pour déployer des ressources dans la région spécifiée.

•	gcloud compute networks subnets create subnet-Region \
 --network taw-custom-network \
 --region Region \
 --range 10.1.0.0/16

La commande crée un sous-réseau personnalisé nommé "subnet-Region" au sein du réseau personnalisé "taw-custom-network", dans une région spécifique, avec une plage d'adresses IP définie (10.1.0.0/16)

•	gcloud compute networks subnets create subnet-Region \
--network taw-custom-network \
--region Region \
--range 10.2.0.0/16

La commande crée un sous-réseau personnalisé nommé "subnet-Region" au sein du réseau personnalisé "taw-custom-network", dans une région spécifique, avec une plage d'adresses IP définie (10.2.0.0/16)

•	gcloud compute networks subnets list \
--network taw-custom-network

La commande liste tous les sous-réseaux associés au réseau spécifié ("taw-custom-network" dans ce cas). Elle peut être utile pour obtenir des informations sur les sous-réseaux existants dans un projet GCP, tels que leurs noms, plages d'adresses IP et autres détails associés.

•	gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http

La commande crée une règle de pare-feu nommée "nw101-allow-http" autorisant le trafic TCP sur le port 80. Cette règle s'applique au réseau personnalisé "taw-custom-network" pour toutes les adresses IP source (0.0.0.0/0). Elle cible les instances étiquetées "http". En résumé, elle permet le trafic HTTP entrant sur les instances VM associées à cette règle dans le réseau spécifié.

•	gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules

La commande crée une règle de pare-feu nommée "nw101-allow-icmp" qui autorise le trafic ICMP dans le réseau personnalisé "taw-custom-network". Cette règle s'applique aux instances de VM étiquetées avec "rules", permettant ainsi les requêtes ICMP telles que le ping.

•	gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"

La commande crée une règle de pare-feu nommée "nw101-allow-internal" qui autorise tout le trafic TCP, UDP et ICMP à l'intérieur du réseau personnalisé "taw-custom-network". Cette règle s'applique aux plages d'adresses IP source spécifiées, permettant ainsi la communication interne entre les ressources du réseau.

•	gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"

La commande crée une règle de pare-feu nommée "nw101-allow-ssh" qui autorise le trafic TCP sur le port 22 (SSH). Cette règle s'applique aux instances de VM étiquetées avec "ssh", permettant ainsi l'accès SSH aux ressources associées dans le réseau spécifié.

•	gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"

La commande crée une règle de pare-feu nommée "nw101-allow-rdp" qui autorise le trafic TCP sur le port 3389 (RDP). Cette règle s'applique au réseau personnalisé "taw-custom-network", permettant ainsi l'accès RDP aux ressources associées dans le réseau spécifié.
